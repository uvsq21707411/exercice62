package exercice62;


public final class Fraction {

	private final int num;
	private final int den;
    public static final Fraction ZERO = new Fraction(0,1);
    public static final Fraction UN = new Fraction(1,1);
	
	public Fraction(int n,int d) {
		num = n;
		den = d;
	}
	public Fraction(int n) {
		num = n;
		den = 1;
	}
	public Fraction() {
		num = 0;
		den = 1;
	}
	
	
	public int getden() {
		return den;
	}
	
	public int getnum() {
		return num;
	}
	
	public double valeur() {
		double a = (double)num;
		double b = (double)den;
		return (a/b);
	}
@Override
	public String toString() {
		return num+"/"+den;
	}
	
	public boolean egal(Fraction f) {
		return this.valeur() == f.valeur();
	}
}
