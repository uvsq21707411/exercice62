package exercice62;

import static org.junit.Assert.*;

import org.junit.Test;

public class FractionTest {

	@Test
	public void testgetden() {
		Fraction f1 = new Fraction(4,2);
		assertTrue(f1.getden()==2);
	
	}
	
	@Test
	public void testgetnum()  {
		Fraction f2 = new Fraction(5,2);
		assertTrue(f2.getnum()==5);
	}
	
	@Test
	public void testvaleur()  {
		Fraction f3 = new Fraction(7,2);
		assertTrue(f3.valeur()==3.5);
	}
	
	@Test
	public void testtoString() {
		Fraction f3 = new Fraction(7,2);
		assertEquals(f3.toString(),"7/2");
	}


}

